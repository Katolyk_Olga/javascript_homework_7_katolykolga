// 1. Опишіть своїми словами як працює метод forEach.
// За допомогою метода forEach ми можемо застосувати певну задану функцію до всіх елементів масива по черзі

// 2. Як очистити масив?
// Очистити масив можна кількома способами. Перший, це зменшити довжину (кількість елементів) length.
// Другий, це вирізати методом splice потрібний діапазон елементів.
// Третій спосіб, це профільтрувати масив на місці, залишивши тільки елементи, що задовольняють умові фільтру.

// 3. Як можна перевірити, що та чи інша змінна є масивом?
// Для перевірки чи масив змінна, потрібна до неї застосувати метод Array.isArray(value).
// Метод повертає true або falce

// Реалізувати функцію фільтру масиву за вказаним типом даних. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// + Написати функцію filterBy(), яка прийматиме 2 аргументи. 
// + Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// + Функція повинна повернути новий масив, який міститиме всі дані, 
// які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. 
// + Тобто якщо передати масив ['hello', 'world', 23, '23', null], 
// і другим аргументом передати 'string', то функція поверне масив [23, null].

let arr = ['hello', 'world', 23, '23', null, true, 0, undefined, {}];

const filterBy = function(arr, dataType) {
    return arr.filter(item => typeof item !== dataType)
};

console.log(arr);
// console.log(filterBy(arr, "string"));
console.log(`Array does not include string typeof: `, filterBy(arr, "string"));
console.log(`Array does not include boolean typeof: `, filterBy(arr, "boolean"));
console.log(`Array does not include number typeof: `, filterBy(arr, "number"));
// console.log(`filterBy(arr, "bolean")`);
// console.log(`filterBy(arr, "number")`);
